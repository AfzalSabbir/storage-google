<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />

    <title>Google Drive</title>
  </head>
  <body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <form action="{{ asset('/upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{--<label for="" class="form-group">
                        Upload
                        <input class="form-control" type="file" name="file" />
                    </label>--}}

                    <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input" id="customFile"  name="file">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-success d-block">
                </form>
            </div>
            <div class="col-12">
                <table class="table table-striped table-hover table-bordered mt-5">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Size</th>
                            <th>Created</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($allFiles as $file)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$file['filename']}}</td>
                            <td>{{$file['mimetype']}}</td>
                            <td>{{round($file['size']/1024, 2)}}KB</td>
                            <td>{{\Carbon\Carbon::parse($file['timestamp'])->diffForHumans()}}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-primary" target="_blank" href="{{\Storage::disk('google')->url($file['path'])}}">View</a>
                                    <a class="btn btn-sm btn-danger" href="{{ asset("/delete-file/".$file['path']) }}">Delete</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>
