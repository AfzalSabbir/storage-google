<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $files = Storage::disk('google')->listContents();
    $allFiles = collect($files)->sortBy('timestamp')->reverse();
    return view('welcome', compact('allFiles'));
});
Route::get('/get-url/{file}', function ($file) {
    $url = Storage::disk('google')->url($file);
    dd($file, $url);
    return view('welcome');
});
Route::get('/get-file/{file}', function ($file) {
    $url = Storage::disk('google')->get($file);
    dd($file, $url);
    return view('welcome');
});
Route::get('/has-file/{file}', function ($file) {
    $url = Storage::disk('google')->has('1sHBlH5K0uc7pRuxJMzMkIuH3yMjI1k59/'.$file);
    dd($file, $url);
    return view('welcome');
});
Route::get('/delete-file/{file}', function ($file) {
    $delete = Storage::disk('google')->delete($file);
    return redirect()->back();
});
Route::get('/get-files', function () {
    $files = Storage::disk('google')->listContents();
    $collected = collect($files);
    dd($collected);
    return view('welcome');
});

Route::post('/upload', function (Request $request) {
    $file = $request->file('file');
    $file_name = time().'__'.($request->file('file')->getClientOriginalName());
    // $file_name = $request->file('file')->getClientOriginalName();

    $response = Storage::disk('google')->put($file_name, fopen($file, 'r+'));
    return redirect()->back();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
